#!/bin/perl

use strict;
use warnings;

die "Usage: $0 <num_of_vertices> <start_vertex>\n" if @ARGV < 2;

srand(time());
my $vertices = $ARGV[0];
my $edges = $vertices * ($vertices - 1);
my $start = $ARGV[1];

print "GRAPH_E $vertices $edges $start |\n";
for (my $i = 0; $i < $vertices; $i++) {
    for (my $j = 0; $j < $vertices; $j++) {
        if ($i != $j) {
            print "$i $j ".(int rand(100000))." \n";
        }
    }
}
