#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define __GRAPH_C__
#include "graph.h"

enum graph_err graph_errno;

#define NONSPACE(arg) (arg != ' ' && arg != '\n' && arg != '\t')
#define ASPACE(arg) (arg == ' ' || arg == '\n' || arg == '\t')
#define SIGNLEN 7

static char *read_file_as_string(char *filename)
{
    FILE *f = fopen(filename, "r");
    if (!f) {
        if (errno == ENOENT)
            graph_errno = GRAPH_ERR_NO_SUCH_FILE;
        else
            graph_errno = GRAPH_ERR_UNKNOWN;
        return 0;
    }
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *string = malloc(fsize + 1);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = 0;
    return string;
}

static char *get_token_by_index(char *file, int index)
{
    int l = 0, on_word = 0;
    while (file[l]) {
        if (NONSPACE(file[l]))
            on_word = 1;
        if (index == 0 && on_word)
            break;
        if (NONSPACE(file[l]) && ASPACE(file[l + 1])) {
            on_word = 0;
            index--;
        }
        l++;
    }
    if (!file[l])
        return 0;
    return &file[l];
}

static char *get_next_token(char *file)
{
    if (!file)
        return 0;
    int index = 1;
    int l = 0, on_word = 0;
    while (file[l]) {
        if (NONSPACE(file[l]))
            on_word = 1;
        if (index == 0 && on_word)
            break;
        if (NONSPACE(file[l]) && ASPACE(file[l + 1])) {
            on_word = 0;
            index--;
        }
        l++;
    }
    if (!file[l])
        return 0;
    return &file[l];
}

static int check_valid_signe(char *file)
{
    char buff[SIGNLEN + 1];
    strncpy(buff, file, SIGNLEN);
    if (strncmp("GRAPH_E", buff, SIGNLEN))
        return 0;
    return 1;
}

static int check_valid_signm(char *file)
{
    char buff[SIGNLEN + 1];
    strncpy(buff, file, SIGNLEN);
    if (strncmp("GRAPH_M", buff, SIGNLEN))
        return 0;
    return 1;
}

static int get_v_cnt(char *file)
{
    char *word = get_token_by_index(file, 1);
    if (!word)
        return -1;
    return atoi(word);
}

static int get_e_cnt(char *file)
{
    char *word = get_token_by_index(file, 2);
    if (!word)
        return -1;
    return atoi(word);
}

static int get_start(char *file)
{
    char *word = get_token_by_index(file, 3);
    if (!word)
        return -1;
    return atoi(word);
}

edge_t *get_edges(char *file, int e_cnt)
{
    char *word = get_token_by_index(file, 4);
    if (strncmp(word, "|", 1))
        return 0;
    edge_t *e = calloc(e_cnt, sizeof(edge_t));
    char *val1, *val2, *val3;
    for (int i = 0; ; i++) {
        word = val1 = get_next_token(word);
        word = val2 = get_next_token(word);
        word = val3 = get_next_token(word);
        if (!(val1 || val2 || val3))
            break;
        if (!(val1 && val2 && val3)) {
            free(e);
            return 0;
        }
        e[i] = (edge_t) {atoi(val1), atoi(val2), atoi(val3)};
    }

    return e;
}

int **get_matrix(char *file, int v_cnt)
{
    char *word = get_token_by_index(file, 4);
    if (strncmp(word, "|", 1))
        return 0;
    int **e = calloc(v_cnt, sizeof(int *));
    for (int i = 0; i < v_cnt; i++) {
        e[i] = malloc(v_cnt * sizeof(int));
        for (int j = 0; j < v_cnt; j++) {
            if ((word = get_next_token(word)) == 0)
                goto get_matrix_error;
            e[i][j] = atoi(word);
        }
    }
    return e;
get_matrix_error:
    for (int i = 0; i < v_cnt; ++i)
        if (e[i])
            free(e[i]);
    free(e);
    return 0;
}

static int check_valid_data_e(graphe_t *g)
{
    if (g->v_cnt - 1 < g->start ||
            g->v_cnt < 1 ||
            g->e_cnt < 1 ||
            g->start < 0 ||
            g->e == 0)
        return 0;
    else
        return 1;
}

static int check_valid_data_m(graphm_t *g)
{
    if ((g->v_cnt - 1 < g->start) ||
            g->v_cnt < 1 ||
            g->start < 0 ||
            g->m == 0)
        return 0;
    for (int i = 0; i < g->v_cnt; ++i)
        if (!g->m[i])
            return 0;
    return 1;
}

graphe_t *graphe_from_file(char *filename)
{
    char *file = read_file_as_string(filename);
    if (!file)
        return 0;
    graphe_t *g = graphe_load(file);
    free(file);
    return g;
}

graphm_t *graphm_from_file(char *filename)
{
    char *file = read_file_as_string(filename);
    if (!file)
        return 0;
    graphm_t *g = graphm_load(file);
    free(file);
    return g;
}

graphe_t *graphe_load(char *data)
{
    graphe_t *g = calloc(1, sizeof(graphe_t));
    if (!g) {
        graph_errno = GRAPH_ERR_MALLOC;
        return 0;
    }
    if (!check_valid_signe(data)) {
        graph_errno = GRAPH_ERR_INVALID_SIGN;
        goto graphe_load_err;
    }

    g->v_cnt = get_v_cnt(data);
    g->e_cnt = get_e_cnt(data);
    g->start = get_start(data);
    g->e = get_edges(data, g->e_cnt);

    if (!check_valid_data_e(g)) {
        graph_errno = GRAPH_ERR_INVALID_DATA;
        goto graphe_load_err;
    }
    graph_errno = GRAPH_OK;
    return g;
graphe_load_err:
    graphe_free(g);
    return 0;
}

graphm_t *graphm_load(char *data)
{
    graphm_t *g = calloc(1, sizeof(graphm_t));
    if (!g) {
        graph_errno = GRAPH_ERR_MALLOC;
        return 0;
    }
    if (!check_valid_signm(data)) {
        graph_errno = GRAPH_ERR_INVALID_SIGN;
        goto graphm_load_err;
    }

    g->v_cnt = get_v_cnt(data);
    g->e_cnt = get_e_cnt(data);
    g->start = get_start(data);
    g->m = get_matrix(data, g->v_cnt);

    if (!check_valid_data_m(g)) {
        graph_errno = GRAPH_ERR_INVALID_DATA;
        goto graphm_load_err;
    }
    graph_errno = GRAPH_OK;
    return g;
graphm_load_err:
    graphm_free(g);
    return 0;
}

void graphe_free(graphe_t *g)
{
    if (g) {
        if (g->e)
            free(g->e);
        free(g);
    }
}

void graphm_free(graphm_t *g)
{
    if (g) {
        if (g->m) {
            for (int i = 0; i < g->v_cnt; ++i)
                if (g->m[i])
                    free(g->m[i]);
            free(g->m);
        }
        free(g);
    }
}
