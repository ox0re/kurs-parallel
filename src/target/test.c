#include <stdio.h>
#include <munit.h>

#include "graph.h"

/*
 * Test load_edges
 *
 * */

static void *
test_load_edges_setup(const MunitParameter params[], void *user_data)
{
    (void) params;
    (void) user_data;
    graphe_t *g = malloc(sizeof(graphe_t));
    *g = (graphe_t) {
        .v_cnt = 5,
        .e_cnt = 6,
        .start = 0,
        .e = malloc(6 * sizeof(edge_t))
    };
    edge_t e[6] = {
        {4, 0, 20},
        {0, 1, 10},
        {1, 2, 10},
        {2, 3, 10},
        {0, 3, 10},
        {3, 4, 5}
    };
    memcpy(g->e, e, 6 * sizeof(edge_t));
    return g;
}

static MunitResult
test_load_edges(const MunitParameter params[], void *fixture)
{
    (void) params;
    graphe_t *expected = (graphe_t *)fixture;
    graphe_t *g = graphe_from_file("graph_e.gp");

    munit_assert_int(g->v_cnt, ==, expected->v_cnt);
    munit_assert_int(g->e_cnt, ==, expected->e_cnt);
    munit_assert_int(g->start, ==, expected->start);
    for (int i = 0; i < g->e_cnt; ++i) {
        munit_assert_int(g->e[i].a, ==, expected->e[i].a);
        munit_assert_int(g->e[i].b, ==, expected->e[i].b);
        munit_assert_int(g->e[i].cost, ==, expected->e[i].cost);
    }

    graphe_free(g);
    return MUNIT_OK;
}

static void
test_load_edges_teardown(void *fixture)
{
    graphe_free(fixture);
}

/*
 * Test load_matrix
 *
 * */

static void *
test_load_matrix_setup(const MunitParameter params[], void *user_data)
{
    (void) params;
    (void) user_data;
    graphm_t *g = malloc(sizeof(graphm_t));
    *g = (graphm_t) {
        .v_cnt = 5,
        .e_cnt = 6,
        .start = 1,
        .m = malloc(5 * sizeof(int *))
    };
    for (int i = 0; i < g->v_cnt; ++i)
        g->m[i] = malloc(g->v_cnt * sizeof(int));
    int m[5][5] = {
        { 0, 10, -1, 10, -1},
        {-1,  0, 10, -1, -1},
        {-1, -1,  0, 10, -1},
        {-1, -1, -1,  0,  5},
        {20, -1, -1, -1,  0},
    };
    for (int i = 0; i < g->v_cnt; ++i)
        memcpy(g->m[i], m[i], g->v_cnt * sizeof(int));
    return (void *) g;
}

static MunitResult
test_load_matrix(const MunitParameter params[], void *fixture)
{
    (void) params;
    graphm_t *expected = (graphm_t *)fixture;
    graphm_t *g = graphm_from_file("graph_m.gp");

    munit_assert_int(g->v_cnt, ==, expected->v_cnt);
    munit_assert_int(g->e_cnt, ==, expected->e_cnt);
    munit_assert_int(g->start, ==, expected->start);
    for (int i = 0; i < g->v_cnt; ++i)
        for (int j = 0; j < g->v_cnt; ++j)
            munit_assert_int(g->m[i][j], ==, expected->m[i][j]);

    graphm_free(g);
    return MUNIT_OK;
}

static void
test_load_matrix_teardown(void *fixture)
{
    graphm_free(fixture);
}

/*
 * Test graphe invalid sign
 *
 * */

static void *
test_graphe_invalid_sign_setup(const MunitParameter params[], void *user_data)
{
    (void) params;
    (void) user_data;
    char **fixture = malloc(3 * sizeof(char *));
    fixture[0] = malloc(24 * sizeof(char));
    fixture[1] = malloc(2 * sizeof(char));
    fixture[2] = malloc(1 * sizeof(char));
    memcpy(fixture[0], "GRAPH_A 2 2 0 | 0 1 10 \0", 24);
    memcpy(fixture[1], "G\0", 2);
    memcpy(fixture[2], "\0", 1);
    return (void *) fixture;
}

static MunitResult
test_graphe_invalid_sign(const MunitParameter params[], void *fixture)
{
    (void) params;
    char **lfixture = (char **)fixture;

    graphe_t *g = graphe_load(lfixture[0]);
    munit_assert_null(g);
    munit_assert_int(graph_errno, ==, GRAPH_ERR_INVALID_SIGN);
    g = graphe_load(lfixture[1]);
    munit_assert_null(g);
    munit_assert_int(graph_errno, ==, GRAPH_ERR_INVALID_SIGN);
    g = graphe_load(lfixture[2]);
    munit_assert_null(g);
    munit_assert_int(graph_errno, ==, GRAPH_ERR_INVALID_SIGN);

    return MUNIT_OK;
}

static void
test_graphe_invalid_sign_teardown(void *fixture)
{
    char **lfixture = (char **)fixture;
    for (int i = 0; i < 3; ++i)
        free(lfixture[i]);
    free(lfixture);
}

/*
 * Test graphm invalid sign
 *
 * */

static void *
test_graphm_invalid_sign_setup(const MunitParameter params[], void *user_data)
{
    (void) params;
    (void) user_data;
    char **fixture = malloc(3 * sizeof(char *));
    fixture[0] = malloc(24 * sizeof(char));
    fixture[1] = malloc(2 * sizeof(char));
    fixture[2] = malloc(1 * sizeof(char));
    memcpy(fixture[0], "GRAPH_A 2 2 0 | 0 1 10 \0", 24);
    memcpy(fixture[1], "G\0", 2);
    memcpy(fixture[2], "\0", 1);
    return (void *) fixture;
}

static MunitResult
test_graphm_invalid_sign(const MunitParameter params[], void *fixture)
{
    (void) params;
    char **lfixture = (char **)fixture;

    graphm_t *g = graphm_load(lfixture[0]);
    munit_assert_null(g);
    munit_assert_int(graph_errno, ==, GRAPH_ERR_INVALID_SIGN);
    g = graphm_load(lfixture[1]);
    munit_assert_null(g);
    munit_assert_int(graph_errno, ==, GRAPH_ERR_INVALID_SIGN);
    g = graphm_load(lfixture[2]);
    munit_assert_null(g);
    munit_assert_int(graph_errno, ==, GRAPH_ERR_INVALID_SIGN);

    return MUNIT_OK;
}

static void
test_graphm_invalid_sign_teardown(void *fixture)
{
    char **lfixture = (char **)fixture;
    for (int i = 0; i < 3; ++i)
        free(lfixture[i]);
    free(lfixture);
}

/*
 * Suite description
 *
 * */

static MunitTest
test_suite_tests[] = {
    {
        (char *) "/graphe_from_file",
        test_load_edges,
        test_load_edges_setup,
        test_load_edges_teardown,
        MUNIT_TEST_OPTION_NONE,
        NULL
    },
    {
        (char *) "/graphm_from_file",
        test_load_matrix,
        test_load_matrix_setup,
        test_load_matrix_teardown,
        MUNIT_TEST_OPTION_NONE,
        NULL
    },
    {
        (char *) "/graphe_invalid_sign",
        test_graphe_invalid_sign,
        test_graphe_invalid_sign_setup,
        test_graphe_invalid_sign_teardown,
        MUNIT_TEST_OPTION_NONE,
        NULL
    },
    {
        (char *) "/graphm_invalid_sign",
        test_graphm_invalid_sign,
        test_graphm_invalid_sign_setup,
        test_graphm_invalid_sign_teardown,
        MUNIT_TEST_OPTION_NONE,
        NULL
    },
    { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
    (char *) "",
    test_suite_tests,
    NULL,
    1,
    MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char **argv)
{
    return munit_suite_main(&test_suite, NULL, argc, argv);
}
