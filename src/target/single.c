#define _POSIX_C_SOURCE 199309L
#include <time.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include "graph.h"

typedef struct Argument {
    graphm_t *g;
    int *d, *p;
} Argument;

const int INF = INT_MAX;

void *solve(Argument data)
{
    graphm_t *g = data.g;
    //int edges = g->edgesCount;
    int vertices = g->v_cnt;
    int **m = g->m;

    int *d = data.d; // содержит длину пути от начальной вершины до каждой
    int *p = data.p; // содержит путь от начальной вершины до каждой
    for (;;) {
        char changed = 0;
        for (int i = 0; i < vertices; ++i)
            for (int j = 0; j < vertices; ++j)
                if (d[i] < INF && m[i][j] >= 0)
                    if (d[j] > d[i] + m[i][j]) {
                        d[j] = d[i] + m[i][j];
                        p[j] = i;
                        changed = 1;
                    }
        if (!changed)
            break;
    }
    return NULL;
}

double test(graphm_t *g)
{
    struct timespec start, end;
    double elapsed;
    Argument a = {
        .g = g,
        .p = calloc(g->v_cnt, sizeof(int)),
        .d = calloc(g->v_cnt, sizeof(int))
    };
    for (int i = 0; i < g->v_cnt; i++)
        a.d[i] = INF;
    a.d[g->start] = 0;
    for (int i = 0; i < g->v_cnt; i++)
        a.p[i] = -1;
    clock_gettime(CLOCK_MONOTONIC, &start);
    solve(a);
    clock_gettime(CLOCK_MONOTONIC, &end);
    elapsed = end.tv_sec - start.tv_sec;
    elapsed += (end.tv_nsec - start.tv_nsec) / 1000000000.0;
#ifdef _PRINT_GRAPH
    // === Вывод ===
    for (int t = 0; t < g->v_cnt; t++) {
        printf("Path from %d to %d: %d | ", g->start, t, a.d[t]);
        if (a.d[t] == INF)
            printf("No path from %d to %d.\n", g->start, t);
        else {
            int *path = calloc(1, sizeof(int));
            int pSize = 0;
            for (int cur = t; cur != -1; cur = a.p[cur]) {
                path = realloc(path, (pSize + 1) * sizeof(int));
                path[pSize] = cur;
                ++pSize;
            }
            for (; pSize > 0; pSize--)
                printf("%d ", path[pSize - 1]);
            printf("\n");
            free(path);
        }
    }
#endif
    free(a.p);
    free(a.d);
    return elapsed;
}

char *gatherFromPipe()
{
    size_t size = BUFSIZ, ptr = 0;
    char *memory = malloc(size * sizeof(char));
    while ((memory[ptr++] = getchar()) != EOF)
        if (ptr >= size)
            memory = realloc(memory, size += BUFSIZ);
    memory[--ptr] = '\0';
    return memory;
}

int main(int argc, char **argv)
{
    /* Read input */
    char *data = gatherFromPipe();
    int times;
    if (argc > 1) {
        times = atoi(argv[1]);
    } else {
        printf( "Error: invalid arguments\n"
                "\tUsage: ./single <times>\n"
                "\tExample: ./single 100 <graph_m.gp\n");
        return -1;
    }
    if (times < 1) {
        printf("Number of times must be > 0, but given: %d\n", times);
        return -1;
    }
    graphm_t *g = graphm_load(data);
    free(data);
    if (!g) {
        fprintf(stderr, "Graph file is invalid or empty\n");
        return -1;
    }

    /* Run */
    double sum = 0;
    for (int j = 0; j < times; j++) {
        double time_used = test(g);
        printf("Time used: %lf s\n", time_used);
        sum += time_used;
    }
    printf("Time average: %lf s\n", sum / times);

    /* Terminate */
    graphm_free(g);
    return 0;
}
