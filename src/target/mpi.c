#define _POSIX_C_SOURCE 199309L
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

#include "graph.h"

typedef struct Argument {
    int *d, *p, changed;
} Argument;

const int INF = INT_MAX;
int rank, world_size;

void exch_intermediate_data(Argument *a, int vertices)
{
    for (int i = 0; i < world_size; ++i)
        MPI_Bcast(&a[i].changed, 1, MPI_INT, i, MPI_COMM_WORLD);
    for (int i = 0; i < world_size; ++i) {
        int start = (vertices / world_size) * i;
        int end = (i != world_size - 1
                        ? (vertices / world_size) * (i + 1)
                        : vertices);
        MPI_Bcast(&a[i].d[start], end - start, MPI_INT, i, MPI_COMM_WORLD);
        MPI_Bcast(&a[i].p[start], end - start, MPI_INT, i, MPI_COMM_WORLD);
    }
}

void solve(graphm_t *g, Argument *a)
{
    int vertices = g->v_cnt;
    int **m = g->m;
    int *d = a->d; // содержит длину пути от начальной вершины до каждой
    int *p = a->p; // содержит путь от начальной вершины до каждой
    for (;;) {
        a[rank].changed = 0;
        for (int i = 0; i < vertices; ++i) {
            int start = (vertices / world_size) * rank;
            int end = (rank != world_size - 1
                            ? (vertices / world_size) * (rank + 1)
                            : vertices);
            for (int j = start; j < end; ++j) {
                if (d[i] < INF && m[i][j] >= 0)
                    if (d[j] > d[i] + m[i][j]) {
                        d[j] = d[i] + m[i][j];
                        p[j] = i;
                        a[rank].changed = 1;
                    }
            }
        }

        exch_intermediate_data(a, vertices);
        int changed = 0;
        for (int i = 0; i < world_size; ++i)
            changed += a[i].changed;
        MPI_Barrier(MPI_COMM_WORLD);
        if (!changed)
            break;
    }
}

#ifdef _PRINT_GRAPH
void print_result(graphm_t *g, int *d, int *p)
{
    for (int t = 0; t < g->v_cnt; t++) {
        printf("Path from %d to %d: %d | ", g->start, t, d[t]);
        if (d[t] == INF) {
            printf("No path from %d to %d.\n", g->start, t);
        } else {
            int *path = calloc(1, sizeof(int));
            int pSize = 0;
            for (int cur = t; cur != -1; cur = p[cur]) {
                path = realloc(path, (pSize + 1) * sizeof(int));
                path[pSize] = cur;
                ++pSize;
            }
            for (; pSize > 0; --pSize)
                printf("%d ", path[pSize - 1]);
            printf("\n");
            free(path);
        }
    }
}
#endif

double test(graphm_t *g)
{
    /* initialize data */
    struct timespec start, end;
    double elapsed;
    Argument *a = calloc(world_size, sizeof(Argument));
    int *p = calloc(g->v_cnt, sizeof(int));
    int *d = calloc(g->v_cnt, sizeof(int));
    for (int i = 0; i < g->v_cnt; i++) {
        d[i] = INF;
        p[i] = -1;
    }
    d[g->start] = 0;
    for (int i = 0; i < world_size; i++)
        a[i] = (Argument) {
            .changed = 0,
            .p = p,
            .d = d
        };

    /* Start */
    clock_gettime(CLOCK_MONOTONIC, &start);
    solve(g, a);
    clock_gettime(CLOCK_MONOTONIC, &end);
    elapsed = end.tv_sec - start.tv_sec;
    elapsed += (end.tv_nsec - start.tv_nsec) / 1000000000.0;
#ifdef _PRINT_GRAPH
    // === Вывод ===
    if (rank == 0) {
        print_result(g, d, p);
    }
#endif

    /* free resources*/
    free(p);
    free(d);
    free(a);
    return elapsed;
}

void exch_initial_data(graphm_t *g, int *times)
{
    MPI_Bcast(times, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&g->v_cnt, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&g->e_cnt, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&g->start, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank) {
        g->m = calloc(g->v_cnt, sizeof(int *));
        for (int i = 0; i < g->v_cnt; ++i)
            g->m[i] = malloc(g->v_cnt * sizeof(int));
    }
    for (int i = 0; i < g->v_cnt; ++i)
        MPI_Bcast(g->m[i], g->v_cnt, MPI_INT, 0, MPI_COMM_WORLD);
}

char *gather_input()
{
    size_t size = BUFSIZ, ptr = 0;
    char *memory = malloc(size * sizeof(char));
    while ((memory[ptr++] = getchar()) != EOF)
        if (ptr >= size)
            memory = realloc(memory, size += BUFSIZ);
    memory[--ptr] = '\0';
    return memory;
}


int main(int argc, char **argv)
{
    /* Initialize MPI */
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    /* Read input */
    graphm_t *g;
    int times;
    if (rank == 0) {
        if (argc > 1) {
            times = atoi(argv[1]);
        } else {
            printf( "Error: invalid arguments\n"
                    "\tUsage: ./mpi <times>\n"
                    "\tExample: mpiexec -n 4 --oversubscribe ./mpi 100 <graph_m.gp\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        if (times < 1) {
            printf("Number of times must be > 0, but given: %d\n", times);
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        char *data = gather_input();
        g = graphm_load(data);
        free(data);
        if (!g) {
            fprintf(stderr, "Graph is invalid or empty\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    } else {
        g = malloc(sizeof(graphm_t));
    }
    if (world_size > 1)
        exch_initial_data(g, &times);

    double sum = 0;
    if (!rank)
        printf("Processes: %d\n", world_size);
    for (int j = 0; j < times; j++) {
        double time_used = test(g);
        if (!rank)
            printf("Time used: %lf s\n", time_used);
        sum += time_used;
    }
    if (!rank)
        printf("Time average: %lf s\n", sum / times);

    /* Terminate */
    graphm_free(g);
    MPI_Finalize();
    return 0;
}
