#define _GNU_SOURCE
#define _POSIX_C_SOURCE 199309L
#include <time.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "graph.h"

typedef struct Argument {
    graphm_t *g;
    int thr, thr_cnt;
    int *d, *p, *changed;
} Argument;

const int INF = INT_MAX;
pthread_barrier_t mybarrier;

void *solve(void *data)
{
    Argument *a = ((Argument *)data);
    graphm_t *g = a->g;
    int thread = a->thr;
    int threads_count = a->thr_cnt;
    int vertices = g->v_cnt;
    int **m = g->m;

    int *d = a->d; // содержит длину пути от начальной вершины до каждой
    int *p = a->p; // содержит путь от начальной вершины до каждой
    for (;;) {
        a->changed[thread - 1] = 0;
        for (int i = 0; i < vertices; ++i) {
            for (int j = thread - 1; j < vertices; j += threads_count) {
                if (d[i] < INF && m[i][j] >= 0)
                    if (d[j] > d[i] + m[i][j]) {
                        d[j] = d[i] + m[i][j];
                        p[j] = i;
                        a->changed[thread - 1] = 1;
                    }
            }
            pthread_barrier_wait(&mybarrier);
        }
        int changed = 0;
        for (int i = 0; i < threads_count; ++i)
            changed += a->changed[i];
        pthread_barrier_wait(&mybarrier);
        if (!changed)
            break;
    }
    pthread_exit(0);
}

double test(graphm_t *g, int threads)
{
    /* initialize data */
    struct timespec start, end;
    double elapsed;
    Argument a[threads];
    a[0] = (Argument) {
        .g = g,
        .thr = 1,
        .thr_cnt = threads,
        .changed = calloc(threads, sizeof(int)),
        .p = calloc(g->v_cnt, sizeof(int)),
        .d = calloc(g->v_cnt, sizeof(int))
    };
    for (int i = 0; i < g->v_cnt; i++)
        a[0].d[i] = INF;
    a[0].d[g->start] = 0;
    for (int i = 0; i < g->v_cnt; i++)
        a[0].p[i] = -1;
    pthread_t bf_thread[threads];
    for (int i = 1; i < threads; i++)
        a[i] = (Argument) {
            .g = g,
            .thr = i + 1,
            .thr_cnt = threads,
            .changed = a[0].changed,
            .p = a[0].p,
            .d = a[0].d
        };

    /* Start */
    pthread_barrier_init(&mybarrier, NULL, threads);
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (int i = 0; i < threads; i++)
        pthread_create(&bf_thread[i], NULL, solve, &a[i]);
    for (int i = 0; i < threads; i++)
        pthread_join(bf_thread[i], NULL);
    clock_gettime(CLOCK_MONOTONIC, &end);
    elapsed = end.tv_sec - start.tv_sec;
    elapsed += (end.tv_nsec - start.tv_nsec) / 1000000000.0;
    pthread_barrier_destroy(&mybarrier);
#ifdef _PRINT_GRAPH
    // === Вывод ===
    for (int t = 0; t < g->v_cnt; t++) {
        printf("Path from %d to %d: %d | ", g->start, t, a[0].d[t]);
        if (a[0].d[t] == INF)
            printf("No path from %d to %d.\n", g->start, t);
        else {
            int *path = calloc(1, sizeof(int));
            int pSize = 0;
            for (int cur = t; cur != -1; cur = a[0].p[cur]) {
                path = realloc(path, (pSize + 1) * sizeof(int));
                path[pSize] = cur;
                ++pSize;
            }
            for (; pSize > 0; pSize--)
                printf("%d ", path[pSize - 1]);
            printf("\n");
            free(path);
        }
    }
#endif

    /* free resources*/
    free(a[0].p);
    free(a[0].d);
    return elapsed;
}

char *gatherFromPipe()
{
    size_t size = BUFSIZ, ptr = 0;
    char *memory = malloc(size * sizeof(char));
    while ((memory[ptr++] = getchar()) != EOF)
        if (ptr >= size)
            memory = realloc(memory, size += BUFSIZ);
    memory[--ptr] = '\0';
    return memory;
}

int main(int argc, char **argv)
{
    /* Read input */
    char *data = gatherFromPipe();
    int threads, times;
    if (argc > 2) {
        threads = atoi(argv[1]);
        times = atoi(argv[2]);
    } else {
        printf( "Error: invalid arguments\n"
                "\tUsage: ./pthreads <threads> <times>\n"
                "\tExample: ./pthreads 4 100\n");
        goto exit_fault;
    }
    if (threads < 1) {
        printf("Number of threads must be > 0, but given: %d\n", threads);
        goto exit_fault;
    }
    if (times < 1) {
        printf("Number of times must be > 0, but given: %d\n", times);
        goto exit_fault;
    }
    graphm_t *g = graphm_load(data);
    free(data);
    if (!g) {
        fprintf(stderr, "Graph file is invalid or empty\n");
        goto exit_fault;
    }

    /* Run */
    for (int i = 0; i < threads; i++) {
        printf("Threads: %d\n", i + 1);
        double sum = 0;
        for (int j = 0; j < times; j++) {
            double time_used = test(g, i + 1);
            printf("Time used: %lf s\n", time_used);
            sum += time_used;
        }
        printf("Time average: %lf s\n", sum / times);
    }

    /* Terminate */
    graphm_free(g);
    return 0;
exit_fault:
    graphm_free(g);
    return -1;
}
