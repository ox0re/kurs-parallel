#!/bin/bash
set -e

num_of_times=100
num_of_processors=$(cat /proc/cpuinfo | awk ' /processor/ { last = $3 }; END { print last + 1 }')
if [ -z "$1" ];
then
  echo "Warning: number of experiments did not set. Falling back to 1."
  num_of_experiments=1;
else
  if [ $1 -lt "1" ];
  then
    echo "Error: number of experiments must be greater than 0!"
    exit -1;
  fi
  num_of_experiments=$1;
fi

if [ -z "$num_of_processors" ];
then
  echo "Error: can not determine number of processors!"
  exit -1;
fi

echo Processors: $num_of_processors > pt.log

echo Pthreads experiments: $num_of_experiments >> pt.log
for experiment in $(seq $num_of_experiments); do
  echo Pthreads experiment $experiment >> pt.log
  graph_size=10000
  starting_point=$(( RANDOM % 10001 ))
  echo graph size: $graph_size, starting point: $starting_point >> pt.log
  ./generate_graph_m.pl $graph_size $starting_point | ./pthreads $num_of_processors $num_of_times >> pt.log
done

grep -E 'size|Pthreads|Threads|Time average' pt.log
