Q=@
QQ=@
MPICC=mpicc

TARGET_SINGLE=single
TARGET_PT=pthreads
TARGET_MPI=mpi
TARGET_TEST=test
MUNIT=deps/munit/munit.c.o

SRC=src
BUILD=build
TARGET=$(BUILD)/target

SOURCES:=$(wildcard $(SRC)/*.c)
OBJECTS:=$(patsubst $(SRC)/%.c,$(BUILD)/%.c.o,$(SOURCES))

INCLUDE+=./include
INCLUDE+=./deps/munit
INCLUDE:=$(patsubst %,-I%,$(INCLUDE))

#COMMONFLAGS+=-fsanitize=address
#COMMONFLAGS+=--coverage

CFLAGS+=$(INCLUDE)
CFLAGS+=$(COMMONFLAGS)
CFLAGS+=-Wall
CFLAGS+=-Wextra
CFLAGS+=-Wpedantic
CFLAGS+=-Wshadow
CFLAGS+=-Wduplicated-branches
CFLAGS+=-Wduplicated-cond
CFLAGS+=-O3
CFLAGS+=-g3
CFLAGS+=-std=c11
#CFLAGS+=-D_PRINT_GRAPH

LDFLAGS+=$(COMMONFLAGS)
LDFLAGS+=-Wl,--gc-section

LDFLAGS_SINGLE+=$(LDFLAGS)

LDFLAGS_PT+=$(LDFLAGS)
LDFLAGS_PT+=-lpthread

LDFLAGS_MPI+=$(LDFLAGS)

LDFLAGS_TEST+=$(LDFLAGS)
LDFLAGS_TEST+=$(MUNIT)

all: $(BUILD)
all: $(TARGET)
all: $(TARGET_SINGLE)
all: $(TARGET_PT)
all: $(TARGET_MPI)
ifndef NOTEST
all: $(MUNIT)
all: $(TARGET_TEST)
endif

$(TARGET_SINGLE): $(OBJECTS) $(TARGET)/$(TARGET_SINGLE).c.o
	$(QQ) echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_SINGLE) -o $@ $^

$(TARGET_PT): $(OBJECTS) $(TARGET)/$(TARGET_PT).c.o
	$(QQ) echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_PT) -o $@ $^

$(TARGET_MPI): $(OBJECTS) $(TARGET)/$(TARGET_MPI).c.o
	$(QQ) echo "  LD      $@"
	$(Q) $(MPICC) $(LDFLAGS_MPI) -o $@ $^

$(TARGET_TEST): $(OBJECTS) $(TARGET)/$(TARGET_TEST).c.o
	$(QQ) echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_TEST) -o $@ $^

$(MUNIT): $(patsubst %.c.o,%.c,$(MUNIT))
	$(QQ) echo "  MPICC   $@"
	$(Q) $(CC) -c -o $@ $<

$(BUILD):
	$(Q) mkdir -p $(BUILD)/target

$(TARGET):
	$(Q) mkdir -p $(TARGET)

$(BUILD)/%.c.o: $(SRC)/%.c
	$(QQ) echo "  CC      $@"
	$(Q) $(CC) $(CFLAGS) -c -o $@ $<

clean: $(BUILD)
clean: $(TARGET)
clean:
ifneq (,$(wildcard $(TARGET_SINGLE)))
	$(QQ) echo "  RM      $(TARGET_SINGLE)"
	$(Q) $(RM) -f $(TARGET_SINGLE)
endif
ifneq (,$(wildcard $(TARGET_PT)))
	$(QQ) echo "  RM      $(TARGET_PT)"
	$(Q) $(RM) -f $(TARGET_PT)
endif
ifneq (,$(wildcard $(TARGET_MPI)))
	$(QQ) echo "  RM      $(TARGET_MPI)"
	$(Q) $(RM) -f $(TARGET_MPI)
endif
ifneq (,$(wildcard $(TARGET_TEST)))
	$(QQ) echo "  RM      $(TARGET_TEST)"
	$(Q) $(RM) -f $(TARGET_TEST)
endif
ifneq (,$(wildcard $(MUNIT)))
	$(QQ) echo "  RM      $(MUNIT)"
	$(Q) $(RM) -f $(MUNIT)
endif
ifneq (,$(wildcard $(TARGET)/*.o))
	$(QQ) echo "  RM      $(wildcard $(TARGET)/*.*)"
	$(Q) $(RM) -f $(wildcard $(TARGET)/*)
endif
ifneq (,$(wildcard $(BUILD)/*.*))
	$(QQ) echo "  RM      $(wildcard $(BUILD)/*.*)"
	$(Q) $(RM) -f $(wildcard $(BUILD)/*.*)
endif

mrproper: clean
	$(QQ) echo "  RM      $(BUILD)"
	$(Q) $(RM) -rf $(BUILD)

.PHONY: clean mrproper all
