typedef struct edge {
	int a, b, cost;
} edge_t;

typedef struct graphe {
    int v_cnt;
    int e_cnt;
    int start;
    edge_t *e;
} graphe_t;

typedef struct graphm {
    int v_cnt;
    int e_cnt;
    int start;
    int **m;
} graphm_t;

graphe_t *graphe_from_file(char *filename);
graphe_t *graphe_load(char *data);
graphm_t *graphm_from_file(char *filename);
graphm_t *graphm_load(char *data);
void graphm_free(graphm_t *g);
void graphe_free(graphe_t *g);

enum graph_err {
    GRAPH_OK = 0,
    GRAPH_ERR_UNKNOWN = 1,
    GRAPH_ERR_NO_SUCH_FILE,
    GRAPH_ERR_MALLOC,
    GRAPH_ERR_INVALID_SIGN,
    GRAPH_ERR_INVALID_DATA,
};

#ifndef __GRAPH_C_
    extern enum graph_err graph_errno;
#endif
