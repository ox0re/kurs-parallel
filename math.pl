#!/bin/perl

use strict;
use warnings;

my @times;
my @averages;
my $thread;
my @i_times;

while (<>) {
    if (/(Threads|Processes): ([0-9]+)/) {
        $thread = $2;
        $i_times[$thread] = 0;
    }
    if (/Time used: ([0-9\.]*)/) {
        $times[$thread][$i_times[$thread]++] = $1;
    }
    if (/Time average: ([0-9\.]*)/) {
        $averages[$thread] = 0;
        for (my $i = 0; $i < $i_times[$thread]; ++$i) {
            $averages[$thread] += $times[$thread][$i];
        }
        $averages[$thread] /= $i_times[$thread];
    }
}

for (my $i = 1; $i <= $thread; ++$i) {
    my $exp_sq = 0;
    for (my $j = 0; $j < $i_times[$i]; ++$j) {
        $exp_sq += $times[$i][$j] ** 2;
    }
    $exp_sq /= $i_times[$i];
    my $disp = sqrt($exp_sq - $averages[$i] ** 2);
    print "Thread: $i\n";
    print "Average: $averages[$i]\n";
    print "Dispersion $disp\n";
}
